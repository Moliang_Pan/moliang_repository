//Task 1
let r=4
let c=r*2*Math.PI
let v=c.toFixed(2)

console.log(v)

//Task 2
let animalString="cameldogcatlizard"
let andString=" and "
let a=animalString.substr(8,3)
let b=animalString.substr(5,3)

console.log(a+andString+b)

//Task 3
let person={
  firstName:"Kanye",
  lastName:"West",
  birthDate:"8 June 1977",
  annualIncome:150000000.00
}

console.log(person.firstName+" "+person.lastName+" was born on "+person.birthDate+" and has a annual income of $"+person.annualIncome)

//Task 4
var number1,number2;
number1=number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);
var number3=number1;
number1=number2;
number2=number3;
//HERE your code to swap the values in number1 and number2
console.log("number1 = " + number1 + " number2 = " + number2);

//Task 5
let year;
let yearNot2015Or2016;
year = 2000;
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016)

//Extra Question
let bestfood=["steak","squid","watermelon","mushroom","persimmon"]
let price=["$20","$12","&8","$3","$6"]
console.log(bestfood[0]+" "+price[0])
console.log(bestfood[1]+" "+price[1])
console.log(bestfood[2]+" "+price[2])
console.log(bestfood[3]+" "+price[3])
console.log(bestfood[4]+" "+price[4])