//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    //Question 1 here 
    let data=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25]
    let numbers={
        pos:"",
        neg:""
    }
    for(let i=0;i<data.length;i++){
        if(data[i]%2==0&&data[i]<0){
            numbers.neg+=data[i]+","
        }
        else if(data[i]%2==1&&data[i]>0){
            numbers.pos+=data[i]+","
        }
    }
    output+="Positive Odd:"+numbers.pos.substr(0,numbers.pos.length-1)+"\n"
    output+="Negative Even:"+numbers.neg.substr(0,numbers.neg.length-1)
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}



function question2(){
    let output = "" 
    let d1=d2=d3=d4=d5=d6=0
    //Question 2 here 
    for(let i=1;i<= 60000;i++){
        num=Math.floor((Math.random() * 6) + 1)
        if(num===1){
            d1++;
        }
        else if(num===2){
            d2++;
        }
        else if(num===3){
            d3++;
        }
        else if(num===4){
            d4++;
        }
        else if(num===5){
            d5++;
        }
        else if(num===6){
            d6++;
        }
    }
    output+="Frequency of die rolls\n"
    for(let i=1;i<=6;i++){
        if(i==1){
            output+=i+":"+d1+"\n"
        }
        if(i==2){
            output+=i+":"+d2+"\n"
        }
        if(i==3){
            output+=i+":"+d3+"\n"
        }
        if(i==4){
            output+=i+":"+d4+"\n"
        }
        if(i==5){
            output+=i+":"+d5+"\n"
        }
        if(i==6){
            output+=i+":"+d6+"\n"
        }
    }
    let outPutArea = document.getElementById("outputArea2")
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    let dice=[0,0,0,0,0,0,0]
    //Question 3 here 
    for(let i=1;i<= 60000;i++){
        num=Math.floor((Math.random() * 6) + 1)
        if(num===1){
            dice[1]++;
        }
        else if(num===2){
            dice[2]++;
        }
        else if(num===3){
            dice[3]++;
        }
        else if(num===4){
            dice[4]++;
        }
        else if(num===5){
            dice[5]++;
        }
        else if(num===6){
            dice[6]++;
        }
    }
    output+="Frequency of die rolls\n"
    for(let i=1;i<=6;i++){
        output+=i+":"+dice[i]+"\n"
    }
    let outPutArea = document.getElementById("outputArea3") 
    outPutArea.innerText = output 
}

function question4(){
    let output = ""    
    //Question 4 here 
    var dice={
        Frequencies:{
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions:""
    }
    for(let i=1;i<= 60000;i++){
        num=Math.floor((Math.random() * 6) + 1)
        if(num===1){
            dice.Frequencies[1]++;
        }
        else if(num===2){
            dice.Frequencies[2]++;
        }
        else if(num===3){
            dice.Frequencies[3]++;
        }
        else if(num===4){
            dice.Frequencies[4]++;
        }
        else if(num===5){
            dice.Frequencies[5]++;
        }
        else if(num===6){
            dice.Frequencies[6]++;
        }
    }
    output+="Frequency of die rolls\n"
    output+="Total rolls:"+dice.Total+"\n"+"Frequencies:\n"
    for(let i=1;i<=6;i++){
        output+=i+":"+dice.Frequencies[i]+"\n"
        if(Math.abs(dice.Frequencies[i]-10000)>100){
            dice.Exceptions+=" "+i
        }
    }
    output+="Exceptions:"+dice.Exceptions
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    let person = {
        name: "Jane",
        income: 127050
    }
    let tax=0
    //Question 5 here 
    if(person.income<=18200){
        tax=0
    }
    else if(person.income<=37000){
        tax=(person.income-18200)*0.19
    }
    else if(person.income<=90000){
        tax=(person.income-37000)*0.325+3572
    }
    else if(person.income<=180000){
        tax=(person.income-90000)*0.37+20797
    }
    else{
        tax=(person.income-180000)*0.45+54097
    }
    output+="Jane's income is: $"+person.income+"\n"
    output+=" and her tax owed is: $"+tax
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}