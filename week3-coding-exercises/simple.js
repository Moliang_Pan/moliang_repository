

let output=""
let testObj = {
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
};

let testObj2={
    colour:"grey",
    hint:["start",123,"end"],
    available:false,
    time:1224
}

function ObjectToHtml(obj){
    let resStr=""
    for(prop in obj){
        resStr+=prop+":"+obj[prop]+"<br/>"
    }
    return resStr
}

let outputArea=document.getElementById("outputArea1")
output+=ObjectToHtml(testObj)
output+=ObjectToHtml(testObj2)
outputArea.innerHTML=output



var outputAreaRef = document.getElementById("outputArea3");
var output2 = "";
function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}

function addition(num1,num2){
    return num1+num2
}

let multiply=function(num1,num2){
    return num1*num2
}
output2 += flexible(addition, 3, 5) + "<br/>";
output2 += flexible(multiply, 3, 5) + "<br/>";
outputAreaRef.innerHTML = output2;

/* Question 3
pseudocode:
define a one parameter function
    define empty string output
    define 2 variables which are called curMin and curMax. They store the the maximum value and minimum one enumerated so far
    assign the curMin and curMax with integer in index 0 of array.
    spawn a for loop the enumerate integers for index 1 to end from the parameter array
        if the integer in current index is larger than curMax, change the value of curMax to the current index integer.
        if the integer in current index is smaller than curMin, change the value of curMin to the current index integer.
    add output curMax, a newline character and curMin
    return output
*/

function extremeValues(array){
    let reValue=""
    let curMin=curMax=array[0]
    for(let i=1;i<array.length;i++){
        if(array[i]>curMax)
            curMax=array[i]
        if(array[i]<curMin)
            curMin=array[i]
    }
    reValue+=curMax+"\n"+curMin
    return reValue
}


var values = [4, 3, 6, 12, 1, 3, 8];
let output3=""
output3+=extremeValues(values)
let outputArea2=document.getElementById("outputArea4")
outputArea2.innerText=output3;